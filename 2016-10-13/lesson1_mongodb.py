import pymongo
from pprint import pprint

DB = pymongo.MongoClient('cip54', 27017)

print(DB)
print(DB.database_names())
print(DB.Wikidata.collection_names())

for database in DB.database_names():
    print(database)
    for collection in DB[database].collection_names():
        print("\t", collection)

persons = DB.Test.Person.find()
pprint(list(persons))
print(5*"*")
persons = DB.Test.Person.find_one()
pprint(persons)
print(type(persons))
p1_name = DB.Test.Person.find_one()['Nachname']
print(p1_name)

testdb = DB.Test.Person
persons = testdb.find({'Anrede': "Herr"})
pprint(list(persons))

WP_articles = DB.Wikipedia.articles
articles = WP_articles.find({'title': "Python"})
pprint(list(articles))

persons = testdb.find_one({'Anrede': "Herr"})
pprint(persons)
print(5*"*")
persons = testdb.find({'Anrede': {'$ne': "Herr"}})
pprint(list(persons))
persons = testdb.find({'Titel': {'$in': ["Dr.", "Prof."]}})
pprint(list(persons))
print(5*"*")
persons = testdb.find({'Titel': {'$nin': ["Dr.", "Prof."]}})
pprint(list(persons))

print(5*"*")
arcticles = WP_articles.find({'category': {'$in': ["Python"]}})
pprint(list(articles))

print(5*"*")
persons = testdb.find({'office': {'$gt': 50}})
pprint(list(persons))
print(5*"*")
persons = testdb.find({'office': {'$lt': 271}})
pprint(list(persons))
print(5*"*")
persons = testdb.find({'office': {'$gt': 50, '$lt': 271}})
pprint(list(persons))
print(5*"*")
persons = testdb.find({'Titel': {'$exists': False}})
pprint(list(persons))

print(5*"*")
articles = WP_articles.find({'redirect': {'$exists': True}})
pprint(list(articles))
print(5*"*")
articles = WP_articles.find({'$where': "this.title.length < 10"})
pprint(list(articles))
print(5*"*")
articles = WP_articles.find({'category.30': {'$exists': True}})
pprint(list(articles))

print(5*"*")
articles = WP_articles.find({}, {'_id': 0, 'title': 1})
pprint(list(articles))
print(5*"*")
articles = WP_articles.find({}, {'title': 1}).limit(3)
pprint(list(articles))
print(5*"*")
articles = WP_articles.find({}, {'title': 1}).skip(3).limit(3)
pprint(list(articles))
print(5*"*")
articles = WP_articles.find({}, {'title': 1}).skip(3).limit(3)
pprint(list(articles))
print(5*"*")
articles = WP_articles.find({}, {'title': 1}).sort('title', 1)
pprint(list(articles))
print(5*"*")
articles = WP_articles.find({}, {'title': 1}).sort('title', -1)
pprint(list(articles))
print(5*"*")
articles = WP_articles.find({}, {'_id': 0, 'title': 1}).sort('_id', -1)
pprint(list(articles))

print(5*"*")
articlessum = WP_articles.count()
print(articlessum)
print(5*"*")
articlessum = WP_articles.find({'$where': "this.title.length < 10"}).count()
print(articlessum)
