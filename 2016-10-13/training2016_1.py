def rev(l):
    for i in range(len(l) // 2):
        l[i], l[len(l) - 1 - i] = l[len(l) - 1 - i], l[i]

def charcounts(s):
    d = {}
    for c in s:
        d.setdefault(c, 0)
        d[c] += 1
    return d

def tupify(l1, l2):
    return list(zip(l1, l2))

def swap(l, i, j):
    l[i], l[j] = l[j], l[i]

def merge(l1, l2):
    return sorted(l1 + l2)

def maxk(l, k):
    return sorted(l)[-k:]

def createlist(n, even):
    return list(range(1 + even, 2 * n + 1, 2))

def fib(n):
    if n < 0:
        raise ValueError("n must be non-negative")
    elif n <= 1:
        return n
    else:
        return fib(n - 2) + fib(n - 1)

l1 = [1, 2, 3, 4, 5]
l2 = list(reversed(l1))
rev(l1)
assert l1 == l2

assert charcounts("hello") == {"h": 1, "e": 1, "l": 2, "o": 1}

assert tupify([1,2,3],["a","b","c"]) == [(1,"a"), (2,"b"), (3,"c")]

assert merge(list(range(0,5,2)), list(range(1,5,2))) == list(range(5))

assert maxk([1,4,6,8,3], 2) == [6,8]

assert createlist(5, True) == [2,4,6,8,10]
assert createlist(5, False) == [1,3,5,7,9]

assert fib(7) == 13
