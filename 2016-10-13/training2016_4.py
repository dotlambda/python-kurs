def avg(l):
    try:
        return sum(l) / len(l)
    except (TypeError, ZeroDivisionError):
        raise ValueError("wrong input")

def printavg(l):
    try:
        print(avg(l))
    except ValueError as e:
        print(e)

printavg([])
printavg([1,"2"])
printavg([1,2,3])
