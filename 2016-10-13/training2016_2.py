from random import random

class Driver:
    def __init__(self, mean_skill, variability):
        self.mean_skill = mean_skill
        self.variability = variability
    
    def __str__(self):
        return "Driver(mean_skill: %.2f, variability: %.2f)" \
            %(self.mean_skill, self.variability)
    
    def current_skill(self):
        return min(max(self.mean_skill \
            + 2 * self.variability * random() - self.variability, \
            0), 1)

class Car:
    def __init__(self, max_speed, driver):
        self.max_speed = max_speed
        self.driver = driver
        
    def __str__(self):
        return "Car(max_speed: %d km/h, driver: %s)" \
            %(self.max_speed, str(self.driver))
    
    def drive(self, km):
        return km / self.max_speed * (1 - driver.current_skill())

def race(cars, km):
    return ((car, car.drive(km)) for car in cars)

class AbstractDriver:
    def __init__(self, mean_skill, variability):
        self.mean_skill = mean_skill
        self.variability = variability

    def current_skill(self):
        raise Exception("you can't call an abstract method")
    
class ConstantDtiver(AbstractDriver):
    def current_skill(self):
        return self.skill

class VariableDriver(AbstractDriver):
    def current_skill(self):
        return min(max(self.mean_skill \
            + 2 * self.variability * random() - self.variability, \
            0), 1)

driver = Driver(0.5, 0.2)
assert 0.3 <= driver.current_skill() <= 0.7
print(driver)

driver = Driver(0.5, 1.0)
assert 0.0 <= driver.current_skill() <= 1.0

car = Car(150, Driver(0.2, 0.1))
print(car.drive(10))
print(car)

d1 = Driver(.9, .2)
d2 = Driver(.8, .3)
c1 = Car(180, d1)
c2 = Car(190, d2)
for car, time in race([c1, c2], 100):
    print("car:", car)
    print("time:", time, "hours")
