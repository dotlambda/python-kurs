#!/usr/bin/env python3

import sys

help_msg = """
Prints the transformed string given as argument.

Usage: /> A3.py CMD STRING

Arguments:
    CMD:        The string transformation. One of:
                'rev':   Reverses the string.
                'upper': Transforms the string to upper case.
                'lower': Transforms the string to lower case.
    STRING:     The input string.
""".strip()

if len(sys.argv) != 3:
    print(help_msg)
else:
    cmd = sys.argv[1]
    string = sys.argv[2]
    if cmd == 'rev':
        print("".join(reversed(string)))
    elif cmd == 'upper':
        print(string.upper())
    elif cmd == 'lower':
        print(string.lower())
    else:
        print(help_msg)
