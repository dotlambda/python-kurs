from math import sqrt, pi, cos, sin
from random import random

def dist(x, y):
    return sqrt(sum((x[i] - y[i]) ** 2 for i in range(len(x))))

def radius(r):
    return pi * r ** 2

def from_polar(r, phi):
    return (r * cos(phi), r * sin(phi))

assert dist((1, 0, 1), (0, 1, 1)) == sqrt(2)

assert radius(1) == pi

l = (from_polar(1, 2 * pi * random()) for _ in range(10000000))
for p in l:
    assert dist((0, 0), p) <= 1
