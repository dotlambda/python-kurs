from functools import reduce

def create_printer(prefix):
    def f(fn):
        with open(fn) as fd:
            for line in fd:
                print(prefix + ":", line, end="")
    return f

def even_ones(l):
    return list(filter(lambda x: x % 2 == 0, l))

def count_uneven_ones(l):
    return reduce(lambda x, y: x + y, map(lambda x: x % 2, l))

create_printer("line")(__file__)

l = list(range(10))
assert even_ones(l) == list(range(0, 10, 2))
assert count_uneven_ones(l) == 5
