#!/usr/bin/env python3

import sys

help_msg = """
Sum up the arguments

Usage: /> prog n1 [n2 ...]

Arguments:
    ni:  The ith argument. Must be a integer or float.
""".strip()

if len(sys.argv) == 1:
    print(help_msg)
else:
    print(sum(map(int, sys.argv[1:])))
