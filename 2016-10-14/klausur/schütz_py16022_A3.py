# Schütz
# 3171097
# py16022

class Address:
    def __init__(self, street=None, number=None, zipcode=None, city=None):
        self.street = street
        self.number = number
        self.zipcode = zipcode
        self.city = city

    def __str__(self):
        return 'Address(%s %d, %d %s)' \
            %(self.steet, self.number, self.zipcode, self.city)

    def sim(self, other):
        if self.zipcode is None and other.zipcode is None:
            return 0
        elif self.zipcode != other.zipcode:
            if self.zipcode is None or other.zipcode is None:
                raise ValueError('necessary attribute not set')
            return 0
        elif self.street is None and other.street is None:
            return 0
        elif self.street != other.street:
            if self.street is None or other.street is None:
                raise ValueError('necessary attribute not set')
            return 1
        elif self.number is None and other.number is None:
            return 2
        elif self.number != other.number:
            if self.number is None or other.number is None:
                raise ValueError('necessary attribute not set')
            return 2
        else:
            return 3

if __name__ == '__main__':
    print(Address('a', 1, 1, 'b').sim(Address('a', None, 1, 'b')))
