# Schütz
# 3171097
# py16022

from operator import itemgetter

def slicestring(s, i, j):
    return s[0], s[-1], s[i:j+1]

def median(mylist):
    mylist = sorted(mylist)
    length = len(mylist)
    if length % 2 == 0:
        return (mylist[length // 2 - 1] + mylist[length // 2]) / 2
    else:
        return mylist[length // 2]

def listcounts(l):
    d = {}
    for elem in l:
        d.setdefault(elem, 0)
        d[elem] += 1
    return d

def valsort(d, reverse):
    return sorted(d.items(), key=itemgetter(1), reverse=reverse)

def numoflines(fn):
    count = 0
    with open(fn) as f:
        for _ in f:
            count += 1
    return count

if __name__ == '__main__':
    assert slicestring('abcdefgh', 2, 4) == ('a', 'h', 'cde')
    
    assert median([2,3,4,1]) == 2.5
    assert median([4,1,2]) == 2
    
    assert listcounts([1,2,3,1,3,1]) == {1: 3, 2: 1, 3: 2}
    
    assert valsort({1: 2, 2: 1, 3: 3}, True) == [(3, 3), (1, 2), (2, 1)]
    
    assert numoflines(__file__) == 45
