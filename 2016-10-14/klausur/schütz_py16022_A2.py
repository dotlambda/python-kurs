# Schütz
# 3171097
# py16022

from functools import reduce
from math import sqrt
from random import random

def sum_sqrt(l):
    return reduce(lambda x, y: x + sqrt(y), l, 0)

def rand(n, bounds):
    minx = bounds[0]
    dx = bounds[2] - minx
    miny = bounds[1]
    dy = bounds[3] - miny
    return [(minx + dx * random(), miny + dy * random()) for _ in range(n)]

if __name__ == '__main__':
    assert sum_sqrt([4, 9, 81]) == 14
    
    print(rand(2, (0, 0, 1, 1)))
