#!/usr/bin/env python3
# Schütz
# 3171097
# py16022

from math import ceil
import sys

def split(text, n):
    if n == 0 or n > len(text):
        raise ValueError('n is out of bounds')
    length = ceil(len(text) / n)
    return [text[i * length:min((i + 1) * length, len(text))] for i in range(n)]

def write(text, fn):
    with open(fn, 'w') as f:
        f.write(text)

if __name__ == '__main__':
    assert split('abc', 2) == ['ab', 'c']
    
    if len(sys.argv) != 4:
        print('hilfetext')
        sys.exit()
    with open(sys.argv[1]) as f:
        splitted = split(f.read(), int(sys.argv[2]))
    for i in range(int(sys.argv[2])):
        write(splitted[i], sys.argv[3] + str(i + 1))
