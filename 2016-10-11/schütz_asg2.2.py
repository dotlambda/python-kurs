from geom import Point, Rectangle

class PointSet:
    def __init__(self):
        self.points = []
    
    def add_point(self, point):
        self.points.append(point)
    
    def get_points(self):
        return [str(p) for p in self.points]
    
    def center(self):
        x = sum(p.x for p in self.points) / len(self.points)
        y = sum(p.y for p in self.points) / len(self.points)
        return Point(x, y)
    
    def extent(self):
        ll = Point(min(p.x for p in self.points), min(p.y for p in self.points))
        ur = Point(max(p.x for p in self.points), max(p.y for p in self.points))
        return Rectangle(ll, ur)

if __name__ == "__main__":
    p1 = Point(0.3, 0.4)
    p2 = Point(2.2, 1.4)
    p3 = Point(1, 1)
    points = PointSet()
    points.add_point(p1)
    points.add_point(p2)
    assert Rectangle(p1, p2).center() == points.center()
    points.add_point(p3)
    assert Rectangle(p1, p2) == points.extent()
