from math import sqrt

class Point:
    """
    a simple 2D point
    usage: p1 = Point(0.0, 0.3)
    attribute:
    x: the x coordinate
    y: the y coordinate
    """

    def __init__(self, x, y):
        self.x = x
        self.y = y
    
    def __str__(self):
        return "Point(x: %f, y: %f)" %(self.x, self.y)
    
    def __eq__(self, other):
        return self.dist(other) == 0
    
    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)
    
    def __sub__(self, other):
        return self + -1 * other
    
    def __rmul__(self, scalar):
        return Point(scalar * self.x, scalar * self.y)
        
    def dist(self, other):
        return sqrt((self.x - other.x) ** 2 + (self.y - other.y) ** 2)
        
    def snap(self):
        """ snaps the coordinates to nearest integer value """
        self.x = round(self.x)
        self.y = round(self.y)

class Rectangle:
    """ simple rectangle """
    
    def __init__(self, ll, ur):
        """
        creates a new rectangle
        ll Point lower left
        ur Point upper right
        """
        if ll.x >= ur.x or ll.y >= ur.y:
            raise ValueError("ll point is not valid")
        self.ll = ll
        self. ur = ur
    
    def __str__(self):
        return "Rectangle(ll: %s, ur: %s)" %(str(self.ll), str(self.ur))
    
    def __eq__(self, other):
        return self.ll == other.ll and self.ur == other.ur
    
    def __contains__(something):
        if type(something) == type(Point):
            return ll.x < point.x and ll.y < point.y and \
                ur.x > point.x and ur.y > point.y
        elif type(something) == type(Rectangle):
            return self.contains(something.ll) and self.contains(something.ur)
        else:
            raise TypeError("something should be a Point or a Rectangle")
    
    def snap(self):
        self.ll.snap()
        self.ur.snap()
    
    def move(self, point):
        self.ll += point
        self.ur += point
    
    def area(self):
        return (self.ur.x - self.ll.x) * (self.ur.y - self.ll.y)
    
    def circumference(self):
        return 2 * (self.ur.x - self.ll.x + self.ur.y - self.ll.y)
    
    def center(self):
        return 0.5 * (self.ll + self.ur)

if __name__ == "__main__":
    p1 = Point(5.0, 4.5)
    print(p1)
    p2 = Point(5.0, 5.0)
    print(p1.dist(p2))
    
    p1.snap()
    print(p1)
    print(p1.dist(p2))
    
    p_ll = Point(0.3, 0.4)
    p_ur = Point(2.2, 1.4)
    r = Rectangle(p_ll, p_ur)
    print(r)
    r.snap()
    print(r)
    print(r.area())
    print(r.center())
    r.move(Point(1,1))
    print(r)
