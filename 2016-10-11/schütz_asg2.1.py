class Flashlight:
    def __init__(self):
        self.battery = 1.0
        self.active = False
    
    def __str__(self):
        running = "Running" if self.active else "Not Running"
        return "Flashlight(battery: %.2f, state: %s)" %(self.battery, running)
        
    def switch_on(self):
        if self.active or self.battery >= .07:
            self.active = True
            self.battery -= .05
    
    def switch_off(self):
        self.active = False

def flashlight_test(flashlight):
    flashlight.switch_off()
    flashlight.switch_on()
    i = 0
    while flashlight.active:
        i += 1
        flashlight.switch_off()
        flashlight.switch_on()
    return i

if __name__ == "__main__":
    flashlight = Flashlight()
    print(flashlight)
    assert flashlight_test(flashlight) == 19
    flashlight.battery = .07
    assert flashlight_test(flashlight) == 1
