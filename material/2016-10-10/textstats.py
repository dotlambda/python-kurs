#!/usr/bin/python

import sys

def usage(error=""):
    print("""Computes statistics on a given text file.
Usage:  /> textstats.py CMD FILENAME [OUTFILE]
Arguments:
  CMD       Statistics of the given text [len | wordnum | wordhist]
            'len':       length of the text
            'wordnum':   number of distinct words
            'wordhist':  word counts and probabilities.
                         ordered by count in decreasing order.
                         Output format:   WORD\\tab COUNT\\tab PROBABILITY
            'all':       Compute all statistics and print the
                         results in order:
                         len, wordnum, emptylines, wordhist (each word a line)
  FILENAME  A text file
  OUTFILE (optional argument)
            If given, write results in OUTFILE. Else,
            print results to stdout.
===============================================================================""")
    print(error)
    sys.exit(1)



def gather_text_stats(fn):
    """
    Calculate some basic statistics
    of text file 'fn'.

    Args:
        fn:  filename.

    Returns: (counts, numofchars, totalwords)
        counts:     dictionary of word counts.
        numofchars: number of chars in the file.
        totalwords: total number of words
    """
    counts = {}
    numofchars = 0
    totalwords = 0

    fp = open(fn, "r")
    print(fp)
    for line in fp:
        line = line.strip().lower()
        numofchars += len(line)
        words = line.split()
        totalwords += len(words)
        for word in words:
            counts.setdefault(word, 0)
            counts[word] += 1 
    fp.close()
    return (counts, numofchars, totalwords)


if __name__ == "__main__":

    arguments = sys.argv[1:]
    if len(arguments) < 2:
        usage("insufficient number of arguments")
    cmd = arguments[0].lower()
    f = arguments[1]

    f_out = None
    if len(arguments) >2:
        f_out = arguments[2]
    out = None
    if f_out == None:
        out = sys.stdout
    else:
        out = open(f_out, "w")

    if cmd not in ["len", "wordnum", "wordhist", "all"]:
        usage("cmd not found, valid cms are len, wordnum, wordhist, all")


    counts, numofchars, totalwords = gather_text_stats(f)


    if cmd == "len" or cmd == "all":
        out.write("# of chars:"+ format(numofchars,",d")+"\n")
    if cmd == "wordnum"or cmd == "all":
        out.write( "# of words:"+format(len(counts),",d")+"\n")
    if cmd == "wordhist"or cmd == "all":
        items = list(counts.items())
        items.sort(key=lambda x: x[1], reverse=True)
        for word, count in items:
            out.write("%s\t%d\t%.4f" % (word, count, count/float(totalwords)))
            out.write("\n")

    out.write("\n")
