from math import sqrt

n = 3

def cossim(a, b):
    return sum(a[i] * b[i] for i in range(n)) / \
        (sqrt(sum(a[i] ** 2 for i in range(n))) * \
        sqrt(sum(b[i] ** 2 for i in range(n))))

assert cossim((1,1,0),(0,0,2)) == 0
