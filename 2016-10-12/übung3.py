import inspect

def create_printer(prefix):
    def f(fn):
        with open(fn) as file:
            for line in file:
                print(prefix + ": " + line, end="")
    return f

f = create_printer("foobar")(__file__)
print(__file__)
