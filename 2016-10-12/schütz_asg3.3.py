from random import randrange

colors = ["red", "green", "blue"]

def colorlist():
    return [colors[randrange(len(colors))] for _ in range(100)]

l = colorlist()
for color in colors:
    print(color + ":", len([x for x in l if x == color]))
