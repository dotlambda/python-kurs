from functools import reduce
from random import randint

def words():
    import re
    return re.findall("\w+", open("mobydick.txt", "r").read().lower())

def count_procedural(l):
    count = 0
    for word in l:
        if word == "of":
            count += 1
    return count

def count_comprehensions(l):
    return len([word for word in l if word == "of"])

def count_higher_order(l):
    return reduce(lambda x, y: x + y, map (lambda word: word == "of", l))

def random_procedural():
    l = []
    for _ in range(10):
        l.append(randint(0, 100))
    return l

def random_comprehensions():
    return [randint(0, 100) for _ in range(10)]

def random_higher_order():
    return list(map(lambda _: randint(0, 100), range(10)))

def product(l):
    return reduce(lambda x, y: x * y, l)

assert count_procedural(words()) \
    == count_comprehensions(words()) \
    == count_higher_order(words())

print(random_procedural())
print(random_comprehensions())
print(random_higher_order())

assert product([1, 2, 3, 4]) == 1 * 2 * 3 * 4
