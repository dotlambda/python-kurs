from functools import reduce

def longest_same_value_seq(l):
    max_value = None
    max_length = 0
    value = None
    length = 0
    for elem in l:
        if elem == value:
            length += 1
        else:
            if length >= max_length:
                max_value = value
                max_length = length
            value = elem
            length = 1
    if length >= max_length:
        max_value = value
        max_length = length
    return max_value, max_length

assert longest_same_value_seq([4,2,5,5,5,4,2,3,3,3,3,4,2,2,2,2]) == (2, 4)
