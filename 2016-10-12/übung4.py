from functools import reduce

def even_ones(l):
    return filter(lambda x: x % 2 == 0, l)

def count_even_ones(l):
    return reduce(lambda x, y: x + y, map(lambda _: 1, even_ones(l)))

assert count_even_ones([1, 2, 3, 4, 5, 6, 7, 9]) == 3
